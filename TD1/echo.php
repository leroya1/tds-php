<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP

        /**EXO 8 Q1 et 2**/
        /*$nom = 'Kamara'; $prenom ='Alwin'; $login = '41';

        echo "<p>Utilisateur $nom $prenom de login $login</p>";

        */

        /**EXO 8 Q3**/
        /*
        $utilisateur = [
            'nom' => 'Kamara',
            'prenom' =>'Alwin',
            'login' => '41',
        ];

        var_dump($utilisateur);

        echo "<p>Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]</p>";
        */

        /**EXO 8 Q4**/
        /*
        $utilisateur1 = [
            'nom' => 'Kamara',
            'prenom' =>'Alwin',
            'login' => '41',
        ];

        $utilisateur2 = [
            'nom' => 'Patrick',
            'prenom' =>'Mahomes',
            'login' => '15',
        ];

        $utilisateur3 = [
            'nom' => 'Justin',
            'prenom' =>'Jefferson',
            'login' => '99',
        ];

        $list_utilisateur = [
            'utilisateur1' => $utilisateur1,
            'utilisateur2' => $utilisateur2,
            'utilisateur3' => $utilisateur3,
        ];

        var_dump($list_utilisateur);

        if($list_utilisateur === []){
            echo "Liste vide";
        }
        else{
            echo "<ul>";
            foreach ($list_utilisateur as $utilisateur) {
                echo "<li>$utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login] </li>";
            }

            echo "</ul>";
        }
        */



        ?>
    </body>
</html> 