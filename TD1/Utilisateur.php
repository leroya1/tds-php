<?php

require_once '../TD2/ConnexionBaseDeDonnees.php';

class Utilisateur {
    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string {
        return $this->nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public function getLogin(): string {
        return $this->login;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(string $login, string $nom, string $prenom) {
        $this->login = substr($login,0,64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }



    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string {
        return "$this->nom $this->prenom de login $this->login";
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau[0],$utilisateurFormatTableau[1],$utilisateurFormatTableau[2]);
    }

    public static function recupererUtilisateurs() {
        $tableau = [];

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateurs');

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $tableau;
    }

}
?>
